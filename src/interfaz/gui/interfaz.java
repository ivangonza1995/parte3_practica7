package interfaz.gui;
import procesamiento.op.ImageOperator;
import org.eclipse.wb.swt.SWTResourceManager;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.*;

import java.awt.SystemColor;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Insets;
import javax.swing.border.Border;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

class RoundedBorder implements Border {

    private int radius;

    RoundedBorder(int radius) {
        this.radius = radius;
    }

    public Insets getBorderInsets(Component c) {
        return new Insets(this.radius+1, this.radius+1, this.radius+2, this.radius);
    }

    public boolean isBorderOpaque() {
        return true;
    } 

    public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
        g.drawRoundRect(x, y, width-1, height-1, radius, radius);
    }
}
 
public class interfaz extends JFrame {
 
	String link1;
	String link2;
	
	BufferedImage crayola; 
   	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField col;
	private JTextField fil;
	private JTextField hilo;
	private JTextField textField;
    /**
     * Launch the application.
     */
    public static void main(String[] args) {
    	
    	
        EventQueue.invokeLater(new Runnable() {
            public void run() {
            	try {
        			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        		} catch (ClassNotFoundException e1) {
        			// TODO Auto-generated catch block
        			e1.printStackTrace();
        		} catch (InstantiationException e1) {
        			// TODO Auto-generated catch block
        			e1.printStackTrace();
        		} catch (IllegalAccessException e1) {
        			// TODO Auto-generated catch block
        			e1.printStackTrace();
        		} catch (UnsupportedLookAndFeelException e1) {
        			// TODO Auto-generated catch block
        			e1.printStackTrace();
        		}  
                try {
                	interfaz frame = new interfaz();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
 
  
    public interfaz() {
 
    	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 992, 450);
        
        contentPane = new JPanel();
        contentPane.setLayout(null);
        //contentPane.setBackground(Color.LIGHT_GRAY);
        setContentPane(contentPane);
 
        JButton btnImg1 = new JButton("Elegir imagen");
        btnImg1.setBounds(90, 275, 120, 33);
        contentPane.add(btnImg1);
        
        JLabel Img1 = new JLabel("Imagen 1");
        Img1.setBounds(21,23,300,225);
        contentPane.add(Img1);
        
        JLabel Img2 = new JLabel("Imagen 2");
        Img2.setBounds(339, 23, 300, 225);
        contentPane.add(Img2);
        
		JSlider slider = new JSlider();
		slider.setMinorTickSpacing(1);
		slider.setValue(0);
		slider.setPaintLabels(true);
		slider.setPaintTicks(true);
		slider.setMaximum(10);
		slider.setBounds(216, 330, 227, 59);
		contentPane.add(slider);
		slider.setVisible(true);
        
        JLabel lblColumnas = new JLabel("|");
        lblColumnas.setBounds(556, 335, 15, 14);
        contentPane.add(lblColumnas);
        
        JLabel lblFilas = new JLabel("_");
        lblFilas.setBounds(556, 304, 15, 14);
        contentPane.add(lblFilas);
        
        col = new JTextField();
        col.setBounds(573, 301, 86, 20);
        contentPane.add(col);
        col.setColumns(10);
        
        fil = new JTextField();
        fil.setBounds(573, 332, 86, 20);
        contentPane.add(fil);
        fil.setColumns(10);
        
        hilo = new JTextField();
        hilo.setBounds(573, 370, 86, 19);
        contentPane.add(hilo);
        hilo.setColumns(10);
        
        JComboBox comboBox = new JComboBox();
        comboBox.addItemListener(new ItemListener() {
        	public void itemStateChanged(ItemEvent e) {
        		String valor = e.getItem().toString();
				//System.out.print(valor);
				if(valor == "Todas" || valor == "#")
					slider.setVisible(true);
				else{
					slider.setVisible(false);
				}
        	}
        });
        comboBox.setModel(new DefaultComboBoxModel(new String[] {"+", "-", "*", "#"}));
        comboBox.setBounds(284, 281, 55, 20);
        contentPane.add(comboBox);
        
        
        JButton btnImg2 = new JButton("Elegir imagen 2");
        btnImg2.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent arg0) {
        		
            	
            	JFileChooser fc=new JFileChooser("C:/Users/Lenovo/Desktop/");
            	 
            	fc.setDialogTitle("Selecciona una imagen");
            	int seleccion=fc.showOpenDialog(contentPane);
            	
            	 
            	
            	if(seleccion==JFileChooser.APPROVE_OPTION){
            	 
            	    //Seleccionamos el fichero
            	    File fichero=fc.getSelectedFile();
            	 
            	    //Insertar la imagen en el Jlabel
            	    ImageIcon imgThisImg = new ImageIcon(fichero.getAbsolutePath());
            	    Image nueva = imgThisImg.getImage();
            	    imgThisImg = new ImageIcon (nueva.getScaledInstance(300, 225,Image.SCALE_SMOOTH));
            	    Img2.setIcon(imgThisImg);
            	    link2=fichero.getAbsolutePath();
            	}
            	
            	Img2.setText("");
        		
        	}
        });
        btnImg2.setBounds(401, 275, 120, 33);
        contentPane.add(btnImg2);
        
        JLabel label = new JLabel("Final");
        label.setBounds(657, 23, 300, 225);
        contentPane.add(label);
        
        JButton btnEjecuta = new JButton("Generar");
        btnEjecuta.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		
        		int number = slider.getValue();
    			float alpha = number*0.1f;
        		
        		ImageOperator imagen = new ImageOperator (link1,link2,Integer.parseInt(col.getText()), Integer.parseInt(fil.getText()));
        		imagen.setAlpha(alpha);
        		int i = comboBox.getSelectedIndex();
        		
        		switch(i)
        		{
        		case 0:
        			slider.setVisible(true);
	        		crayola = imagen.Suma(Integer.parseInt(hilo.getText()));
	        		ImageIcon imgThisImg = new ImageIcon(crayola);
	        		Image nueva = imgThisImg.getImage();
	           	    imgThisImg = new ImageIcon (nueva.getScaledInstance(300, 225,Image.SCALE_SMOOTH));
	           	    label.setIcon(imgThisImg);
	            	    
        			break;
        		case 1:
        			slider.setVisible(true);
        			crayola = imagen.Resta(Integer.parseInt(hilo.getText()));
        			ImageIcon imgThisImg1 = new ImageIcon(crayola);
        			Image nueva1 = imgThisImg1.getImage();
            	    imgThisImg1 = new ImageIcon (nueva1.getScaledInstance(300, 225,Image.SCALE_SMOOTH));
            	    label.setIcon(imgThisImg1);
        			break;
        		case 2:
        			slider.setVisible(true);
        			crayola = imagen.Multi(Integer.parseInt(hilo.getText()));
        			ImageIcon imgThisImg2 = new ImageIcon(crayola);
        			Image nueva2 = imgThisImg2.getImage();
            	    imgThisImg2 = new ImageIcon (nueva2.getScaledInstance(300, 225,Image.SCALE_SMOOTH));
            	    label.setIcon(imgThisImg2);
        			break;
        		case 3:
        			slider.setVisible(true);
        			crayola = imagen.Lineal(Integer.parseInt(hilo.getText()));
        			
        			ImageIcon imgThisImg3 = new ImageIcon(crayola);
        			Image nueva3 = imgThisImg3.getImage();
            	    imgThisImg3 = new ImageIcon (nueva3.getScaledInstance(300, 225,Image.SCALE_SMOOTH));
            	    label.setIcon(imgThisImg3);
        			break;		
        		
        		}
        		
        		btnEjecuta.setText("");
        		
        		String nombre=textField.getText();
        		String rutaFile = "C:/Users/Lenovo/Desktop/";
				rutaFile+=nombre;
				rutaFile+=".jpg";
        		File file = new File (rutaFile);
        	    try {
					ImageIO.write(crayola,"jpg",file);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
        		
        	}
        });
        btnEjecuta.setBounds(735, 348, 89, 33);
        contentPane.add(btnEjecuta);
        
        JButton btnGuardar = new JButton("Guardar");
        btnGuardar.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent arg0) {

        		
        		File file = new File ("C:/Users/danae/Pictures/hola.jpg");
        	    try {
					ImageIO.write(crayola,"jpg",file);
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
        	}
        });
        btnGuardar.setBounds(261, 435, 114, 25); 
        contentPane.add(btnGuardar);
        
        JLabel lblNumHilos = new JLabel("Num. Hilos:");
        lblNumHilos.setBounds(511, 373, 62, 15);
        contentPane.add(lblNumHilos);
        
        textField = new JTextField();
        textField.setColumns(10);
        textField.setBounds(710, 281, 134, 20);
        contentPane.add(textField);
        
        JLabel label_1 = new JLabel("Nombre del Archivo");
        label_1.setBounds(710, 312, 134, 14);
        contentPane.add(label_1);
        
        JLabel label_2 = new JLabel("__________________________________________");
        label_2.setBounds(13, 244, 308, 14);
        contentPane.add(label_2);
        
		JLabel label_3 = new JLabel("__________________________________________");
		label_3.setBounds(649, 244, 308, 14);
		contentPane.add(label_3);
		
		JLabel label_4 = new JLabel("__________________________________________");
		label_4.setBounds(331, 244, 308, 14);
		contentPane.add(label_4);
 
        btnImg1.addActionListener(new ActionListener(){
            public void actionPerformed (ActionEvent e){
            	
            	JFileChooser fc=new JFileChooser("C:/Users/danae/Pictures");
            	int seleccion=fc.showOpenDialog(contentPane);
            	 
            	
            	if(seleccion==JFileChooser.APPROVE_OPTION){
            	 
            	    
            	    File fichero=fc.getSelectedFile();
            	 
            	    ImageIcon imgThisImg = new ImageIcon(fichero.getAbsolutePath());
            	    Image nueva = imgThisImg.getImage();
            	    imgThisImg = new ImageIcon (nueva.getScaledInstance(300, 225,Image.SCALE_SMOOTH));
            	    Img1.setIcon(imgThisImg); 
            	    link1= fichero.getAbsolutePath();
            	}
            	
            	 Img1.setText("");
 
            }
        });
 
    }
}
