package procesamiento.op;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.wb.swt.SWTResourceManager;

public class ImageOperator{
	
	Image imagen1;
	Image imagen2;
	int x, y;
	BufferedImage mb1;
	BufferedImage mb2;
	BufferedImage mbFinal;
	boolean chunkMatrix[];
	int  chunkRows;
	int chunkCols;
	int chunkCounter;
	Coordenada[] bloques;
	int Contadorbloques;
	char operacion;
	int band = 0;
	float Alpha = 0;
	
	public ImageOperator(String i1, String i2, int numColm , int numFilas )
	{
		imagen1 = SWTResourceManager.getImage(i1);
		imagen2 = SWTResourceManager.getImage(i2);
		Rectangle A = imagen1.getBounds();
		Rectangle B = imagen2.getBounds();
		chunkRows = numFilas;
		chunkCols = numColm;
		chunkCounter = numFilas * numColm;
		Contadorbloques = numFilas * numColm;
		if(A.width<B.width)
		{
			x=A.width;
		}
		else
		{
			x=B.width;
		}
		
		if(A.height<B.height)
		{
			y=A.height;
		}
		else
		{
			y=B.height;
		}
		mbFinal = new BufferedImage(x,y,BufferedImage.TYPE_INT_RGB);
		bloques = new Coordenada[chunkCounter];
		chunkMatrix =  new boolean[chunkCounter];
		Bloques();
		try {
			mb1 = ImageIO.read(new File(i1));
			mb2 = ImageIO.read(new File(i2));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	void setoperacion(char o)
	{
		operacion = o;
	}
	
	BufferedImage DameFinal()
	{
		return mbFinal;
	}
	
	void Bloques()
	{
		int total = x/chunkCols;
		int total2 = y/chunkRows;
		int aux = 0;
		int aux2 = 0;
		int contador = 0;
			for(int i = 0; i <= x; i++)
			{
				if(i == total)
				{
					for(int j = 0; j <= y ; j++ )
					{
						if(j == total2)
						{
							
							Coordenada a = new Coordenada(aux,i,aux2,j); 
							bloques[contador] = a;
							chunkMatrix[contador] = false;
							contador++;
							aux2 = aux2 + (y/chunkRows);
							total2 = total2 +(y/chunkRows);
							
						}
					}
					aux2  = 0;
					total = total + x/chunkCols;
					aux = aux + (x/chunkCols);
					total2 = y/chunkRows;
				}
				
			}
	}
	public BufferedImage Suma(int z)
	{
		Chunk a[] = new Chunk[z];
		Lock lock = new Bakery(z);
		for(int i  = 0; i < z ; i++ )
		{
			//int range = (3 - 1) + 1;
			//int j = (int) (Math.random() * range)+1;
			switch(band)
			{
			case 0:
				a[i] = new Chunk(mb1,mb2,mbFinal,'+',this,i,lock);
				band = 1;
				break;
			case 1:
				a[i] = new Chunk(mb1,mb2,mbFinal,'-',this,i,lock);
				band = 2;
				break;
			case 2:
				a[i] = new Chunk(mb1,mb2,mbFinal,'*',this,i,lock);
				band = 3;
				break;
				
			case 3:
				a[i] = new Chunk(mb1,mb2,mbFinal,'#',this,i,lock);
				band = 0;
				break;
			}
			a[i].start();
		}
		for(int i  = 0; i < z ; i++ )
		{
			try {
				a[i].join();
				System.out.println("ID: " + a[i].getId()+ " op - "+a[i].operacion + " - id: "+a[i].id);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return mbFinal;
	}
	public BufferedImage Resta(int z)
	{
		Chunk a[] = new Chunk[z];
		Lock lock = new Bakery(z);
		for(int i  = 0; i < z ; i++ )
		{
			switch(band)
			{
			case 0:
				a[i] = new Chunk(mb1,mb2,mbFinal,'+',this,i,lock);
				band = 1;
				break;
			case 1:
				a[i] = new Chunk(mb1,mb2,mbFinal,'-',this,i,lock);
				band = 2;
				break;
			case 2:
				a[i] = new Chunk(mb1,mb2,mbFinal,'*',this,i,lock);
				band = 3;
				break;
			case 3:
				a[i] = new Chunk(mb1,mb2,mbFinal,'#',this,i,lock);
				band = 0;
				break;
			}
			a[i].start();
		}
		for(int i  = 0; i < z ; i++ )
		{
			try {
				a[i].join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return mbFinal;
	}
	
	public BufferedImage Multi(int z)
	{
		Chunk a[] = new Chunk[z];
		Lock lock = new Bakery(z);
		for(int i  = 0; i < z ; i++ )
		{
			switch(band)
			{
			case 0:
				a[i] = new Chunk(mb1,mb2,mbFinal,'+',this,i,lock);
				band = 1;
				break;
			case 1:
				a[i] = new Chunk(mb1,mb2,mbFinal,'-',this,i,lock);
				band = 2;
				break;
			case 2:
				a[i] = new Chunk(mb1,mb2,mbFinal,'*',this,i,lock);
				band = 3;
				break;
			case 3:
				a[i] = new Chunk(mb1,mb2,mbFinal,'#',this,i,lock);
				band = 0;
				break;
			}
			a[i].start();
		}
		for(int i  = 0; i < z ; i++ )
		{
			try {
				a[i].join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return mbFinal;
	}

	public BufferedImage Lineal(int z)
	{
		Chunk a[] = new Chunk[z];
		Lock lock = new Bakery(z);
		for(int i  = 0; i < z ; i++ )
		{
			switch(band)
			{
			case 0:
				a[i] = new Chunk(mb1,mb2,mbFinal,'+',this,i,lock);
				band = 1;
				break;
			case 1:
				a[i] = new Chunk(mb1,mb2,mbFinal,'-',this,i,lock);
				band = 2;
				break;
			case 2:
				a[i] = new Chunk(mb1,mb2,mbFinal,'*',this,i,lock);
				band = 3;
				break;
			case 3:
				a[i] = new Chunk(mb1,mb2,mbFinal,'#',this,i,lock);
				band = 0;
				break;
			}
			a[i].start();
		}
		for(int i  = 0; i < z ; i++ )
		{
			try {
				a[i].join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return mbFinal;
	}
	
	BufferedImage ultimo()
	{
		return mbFinal;
	}
	int numerodebloques()
	{
		return chunkCounter;
	}
	int numerodebloquesprocesados()
	{
		return Contadorbloques;
	}
	
	public void setAlpha(float a)
	{
		Alpha = a;
	}
}