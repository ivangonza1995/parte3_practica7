package procesamiento.op;

class Coordenada
{
	public int xi;
	public int xf;
	public int yi;
	public int yf;
	Coordenada(int a, int b, int c, int d)
	{
		xi = a;
		xf = b;
		yi = c;
		yf = d;
	}
}