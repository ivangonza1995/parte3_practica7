package procesamiento.op;

import java.awt.Color;
import java.awt.image.BufferedImage;

class Chunk  extends Thread  
{
	ImageOperator datos;
	int id;
	Lock lock;
	int xi;
	int xf;
	int yi;
	int yf;
	BufferedImage mbA;
	BufferedImage mbB;
	BufferedImage mbF;
	char operacion;
	int alfaaa;
	boolean asignado;
	boolean terminar;
	
	void setAlfa(int al)
	{
		alfaaa = al;
	}
	
	Chunk( BufferedImage elmbA,BufferedImage elmbB,BufferedImage elmbF,char ope,ImageOperator a, int idd, Lock z){
		mbA = elmbA;
		mbB = elmbB;
		mbF = elmbF;
		operacion = ope;
		datos = a;
		id = idd;
		lock = z;
		terminar = false;
		alfaaa = (int)Math.round(datos.Alpha);
	}
	void Bloque(Coordenada a)
	{
		this.xi = a.xi;
		this.xf = a.xf;
		this.yi = a.yi;
		this.yf = a.yf;
		asignado = true;
	}
	
	void mostrar()
	{
		System.out.println("\n ID: " + getId() + "| Id Lock: " + id + " | Operacion: " + operacion +
				"\n x: " + xi + " - y: " + yi + 
				"\n In " + xf + " - In " + yf);
	}
	
	
	void  Suma()
	{
		mostrar();
		int rr;
		int gr;
		int br;
		for (int i = xi; i < xf; i++) 
		{
			for (int j = yi; j < yf; j++)
			{
				int rgb1 = mbA.getRGB(i, j);
				Color color1 = new Color(rgb1, true);
				int rgb2 = mbB.getRGB(i, j);
				Color color2 = new Color(rgb2, true);
				int r1 = color1.getRed();
				int g1 = color1.getGreen();
				int b1 = color1.getBlue();
				int r2 = color2.getRed();
				int g2 = color2.getGreen();
				int b2 = color2.getBlue();
				
				if(r1+r2 <= 255)
				{
					 rr = r1+r2;
				}
				else
				{
					rr = 255;
				}
				
				if(g1+g2 <= 255)
				{
					 gr = g1+g2;
				}
				else
				{
					gr = 255;
				}
				
				if(b1+b2 <= 255)
				{
					 br = b1+b2;
				}
				else
				{
					br = 255;
				}
				
				Color a = new Color(rr,gr,br);
				mbF.setRGB(i, j,a.getRGB());	
			}
		}
	}
	
	void Resta()
	{
		mostrar();
		int rr;
		int gr;
		int br;
		for (int i = xi; i < xf; i++) 
		{
			for (int j = yi; j < yf; j++)
			{
				int rgb1 = mbA.getRGB(i, j);
				Color color1 = new Color(rgb1, true);
				int rgb2 = mbB.getRGB(i, j);
				Color color2 = new Color(rgb2, true);
				int r1 = color1.getRed();
				int g1 = color1.getGreen();
				int b1 = color1.getBlue();
				int r2 = color2.getRed();
				int g2 = color2.getGreen();
				int b2 = color2.getBlue();
				
				if(Math.abs(r1-r2) <= 255)
				{
					 rr =Math.abs(r1-r2);
				}
				else
				{
					rr = 255;
				}
				
				if(Math.abs(g1-g2) <= 255)
				{
					 gr = Math.abs(g1-g2);
				}
				else
				{
					gr = 255;
				}
				
				if(Math.abs(b1-b2) <= 255)
				{
					 br = Math.abs(b1-b2);
				}
				else
				{
					br = 255;
				}
				
				Color a = new Color(rr,gr,br);
				mbF.setRGB(i, j,a.getRGB());	
			}
		}
	}
	
	public void run()
	{
		while(true)
		{
			lock.requestCS(id);
			CR();
			lock.releaseCS(id);
			if(terminar == true)
			{
				break;
			}
			if(asignado == true)
			{
				NCR();
			}
			
		}
		
	}
	
	private void NCR() {
	//System.out.println("ID: "+ getId() +" - NCR");
		switch(operacion)
		{
		case '+':
			Suma();
			break;
		case '-':
			Resta();
			break;
		case '*':
			Multi();
			break;
		case '#':
			Lineal();
			break;
		}
		
	}
	private void CR() {
	;
	//System.out.println("ID: "+ getId() +" - EN LA CR");
		if(datos.chunkCounter>0)
		{
			if(asignado == false)
			{
				for(int i = 0; i < datos.Contadorbloques; i++)
				{
					if(datos.chunkMatrix[i] == false)
					{
						datos.chunkMatrix[i] = true;
						Bloque(datos.bloques[i]);
						break;
					}
					if(i == datos.Contadorbloques-1)
					{
						terminar = true;
					}
				}
			}
			else
			{
				datos.chunkCounter--;
				asignado = false;
				
			}
		}
		else
		{
			terminar = true;
		}
		
	}
	void Multi()
	{
		mostrar();
		int rr;
		int gr;
		int br;
		for (int i = xi; i < xf; i++) 
		{
			for (int j = yi; j < yf; j++)
			{
				int rgb1 = mbA.getRGB(i, j);
				Color color1 = new Color(rgb1, true);
				int rgb2 = mbB.getRGB(i, j);
				Color color2 = new Color(rgb2, true);
				int r1 = color1.getRed();
				int g1 = color1.getGreen();
				int b1 = color1.getBlue();
				int r2 = color2.getRed();
				int g2 = color2.getGreen();
				int b2 = color2.getBlue();
				float k = (float)1/(float)255;
					rr =(int) (k*r1*r2);
					gr =(int) (k*g1*g2);
					br = (int) (k*b1*b2);
					if(rr>255)
						rr=255;
					if(gr>255)
						gr=255;
					if(br>255)
						br=255;
				Color a = new Color(rr,gr,br);
				mbF.setRGB(i, j,a.getRGB());	
			}
		}
	}
	void Lineal()
	{
		mostrar();
		int rr;
		int gr;
		int br;
		float alfa=(float) alfaaa/100;
		float beta=(float) (100-alfaaa)/100;
		for (int i = xi; i < xf; i++) 
		{
			for (int j = yi; j < yf; j++)
			{
				int rgb1 = mbA.getRGB(i, j);
				Color color1 = new Color(rgb1, true);
				int rgb2 = mbB.getRGB(i, j);
				Color color2 = new Color(rgb2, true);
				int r1 = color1.getRed();
				int g1 = color1.getGreen();
				int b1 = color1.getBlue();
				int r2 = color2.getRed();
				int g2 = color2.getGreen();
				int b2 = color2.getBlue();
					rr =(int) ((int) (alfa*r1)+(beta*r2));
					gr =(int) ((int) (alfa*g1)+(beta*g2));
					br = (int) ((int) (alfa*b1)+(beta*b2));
					if(rr>255)
						rr=255;
					if(gr>255)
						gr=255;
					if(br>255)
						br=255;
				Color a = new Color(rr,gr,br);
				mbF.setRGB(i, j,a.getRGB());	
			}
		}
	}
	
}