package procesamiento.op;

interface Lock
{
	public void requestCS( int pid ); //puede bloquear
	public void releaseCS( int pid ); //libera la CR
} 
class Bakery implements Lock
{
	int N;
	volatile boolean[] choosing;
	volatile int [] number;
	
	Bakery(int numProc)
	{
		N = numProc;
		choosing = new boolean[N];
		number = new int[N];
		for(int i = 0 ; i < N ; i++)
		{
			choosing[i] = false;
			number[i] =0;
		}
	}
	
	public void requestCS(int i)
	{
		//System.out.println("ID: "+ i +" - REQUESTING CS");
		choosing[i] = true;
		for(int j = 0; j < N; j++)
			if(number[j]>number[i])
				number[i] = number[j];
		number[i]++;
		choosing[i] =  false;
		
		for(int j = 0; j < N; j++)
		{
			while(choosing[j]);
			while((number[j]!=0) && ((number[j]<number[i]) || ((number[j]== number[i])&& j < i)));
		}
	}
	public void releaseCS(int i)
	{
		//System.out.println("ID: " + i + " - RELEASING CS");
		number[i] = 0;
	}	
}